﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;

namespace e_Commerce.Models
{
    public class ProductModel
    {
        [Key]
        public int ProductID { get; set; }
        //
        [StringLength(100)]
        [Display(Name = "Tên Sản Phẩm")]
        public string Name { get; set; }
        //
        [StringLength(100)]
        [Display(Name = "Thông tin tiêu đề")]
        public string Title { get; set; }
        //
        [StringLength(100)]
        [Display(Name = "Mô tả")]
        public string Description { get; set; }
        //
        [StringLength(100)]
        [Display(Name = "Hình Sản Phẩm")]
        public string Photos { get; set; }
        //
        [StringLength(100)]
        [Display(Name = "Nguyên Vật Liệu")]
        public string Materials { get; set; }
        //
        [StringLength(100)]
        [Display(Name = "Màu Sắc")]
        public string Color { get; set; }
        //
        [Display(Name = "Kích cỡ")]
        public string Size { get; set; }
        //
        [Display(Name = "Sô Lượng")]
        public int Quantity { get; set; } = 0;
        //
        [Display(Name = "Giá")]
        public decimal Price { get; set; } = 0;
        //
        public int SubCategoryID { get; set; }
        [ForeignKey("SubCategoryID")]
        public virtual SubCategoryModel SubCategory { get; set; }
        //
        [Display(Name = "Ngày Tạo")]
        public DateTime DateCreate { get; set; }=DateTime.Now;
        //
        [Display(Name = "Sản Phẩm Hot")]
        public bool Features { get; set; }
        //
        [Display(Name = "Trạng Thái")]
        public bool Status { get; set; } = true;
    }
}
