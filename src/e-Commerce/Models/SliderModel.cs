﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace e_Commerce.Models
{
    public class SliderModel
    {
        [Key]
        public int SliderID { get; set; }
        //
        public string Title { get; set; }
        //
        public string Image { get; set; }
        //
        public string Content { get; set; }
        //
        public bool Status { get; set; }
    }
}
