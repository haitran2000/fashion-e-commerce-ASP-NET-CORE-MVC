﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace e_Commerce.Models
{
    public class SubCategoryModel
    {
        [Key]
        public int SubCategoryID { get; set; }
        //
        public string Name { get; set; }
        //
        public int CategoryID { get; set; }
        [ForeignKey("CategoryID")]
        public virtual CategoryModel Category { get; set; }
        //

        public bool Status { get; set; }
    }
}
