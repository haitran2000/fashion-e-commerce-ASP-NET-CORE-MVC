﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace e_Commerce.Service
{
    public class Panel
    {
        public  static string Image(string PhotoName, string Folder,IFormFile ful)
        {
            var path = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/image/"+Folder+"", PhotoName + "." + ful.FileName.Split(".")[ful.FileName.Split(".").Length - 1]);
            using (var stream = new FileStream(path, FileMode.Create))
            {
                ful.CopyToAsync(stream);
            }
              return PhotoName + "." + ful.FileName.Split(".")[ful.FileName.Split(".").Length - 1];
        }
    }
}
