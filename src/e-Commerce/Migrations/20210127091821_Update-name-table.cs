﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace e_Commerce.Migrations
{
    public partial class Updatenametable : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_SubCategoryModel_SubCategoryID",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_SubCategoryModel_Categories_CategoryID",
                table: "SubCategoryModel");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubCategoryModel",
                table: "SubCategoryModel");

            migrationBuilder.RenameTable(
                name: "SubCategoryModel",
                newName: "SubCategory");

            migrationBuilder.RenameIndex(
                name: "IX_SubCategoryModel_CategoryID",
                table: "SubCategory",
                newName: "IX_SubCategory_CategoryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubCategory",
                table: "SubCategory",
                column: "SubCategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_SubCategory_SubCategoryID",
                table: "Products",
                column: "SubCategoryID",
                principalTable: "SubCategory",
                principalColumn: "SubCategoryID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubCategory_Categories_CategoryID",
                table: "SubCategory",
                column: "CategoryID",
                principalTable: "Categories",
                principalColumn: "CategoryID",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Products_SubCategory_SubCategoryID",
                table: "Products");

            migrationBuilder.DropForeignKey(
                name: "FK_SubCategory_Categories_CategoryID",
                table: "SubCategory");

            migrationBuilder.DropPrimaryKey(
                name: "PK_SubCategory",
                table: "SubCategory");

            migrationBuilder.RenameTable(
                name: "SubCategory",
                newName: "SubCategoryModel");

            migrationBuilder.RenameIndex(
                name: "IX_SubCategory_CategoryID",
                table: "SubCategoryModel",
                newName: "IX_SubCategoryModel_CategoryID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_SubCategoryModel",
                table: "SubCategoryModel",
                column: "SubCategoryID");

            migrationBuilder.AddForeignKey(
                name: "FK_Products_SubCategoryModel_SubCategoryID",
                table: "Products",
                column: "SubCategoryID",
                principalTable: "SubCategoryModel",
                principalColumn: "SubCategoryID",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_SubCategoryModel_Categories_CategoryID",
                table: "SubCategoryModel",
                column: "CategoryID",
                principalTable: "Categories",
                principalColumn: "CategoryID",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
