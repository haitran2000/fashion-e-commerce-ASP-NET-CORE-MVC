﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using e_Commerce.Data;
using e_Commerce.Models;

namespace e_Commerce.Areas.Admin.Controllers
{
    [Area("Admin")]
    public class SubCategoryController : Controller
    {
        private readonly DPContext _context;

        public SubCategoryController(DPContext context)
        {
            _context = context;
        }

        // GET: Admin/SubCategory
        public async Task<IActionResult> Index()
        {
            return View(await _context.SubCategory.ToListAsync());
        }

        // GET: Admin/SubCategory/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var SubCategory = await _context.SubCategory
                .FirstOrDefaultAsync(m => m.SubCategoryID == id);
            if (SubCategory == null)
            {
                return NotFound();
            }

            return View(SubCategory);
        }

        // GET: Admin/SubCategory/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: Admin/SubCategory/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("SubCategoryID,Name,CategoryID,Status")] SubCategoryModel SubCategory)
        {
            if (ModelState.IsValid)
            {
                _context.Add(SubCategory);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(SubCategory);
        }

        // GET: Admin/SubCategory/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var SubCategory = await _context.SubCategory.FindAsync(id);
            if (SubCategory == null)
            {
                return NotFound();
            }
            return View(SubCategory);
        }

        // POST: Admin/SubCategory/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("SubCategoryID,Name,CategoryID,Status")] SubCategoryModel SubCategory)
        {
            if (id != SubCategory.SubCategoryID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(SubCategory);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!SubCategoryExists(SubCategory.SubCategoryID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(SubCategory);
        }

        // GET: Admin/SubCategory/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var SubCategory = await _context.SubCategory
                .FirstOrDefaultAsync(m => m.SubCategoryID == id);
            if (SubCategory == null)
            {
                return NotFound();
            }

            return View(SubCategory);
        }

        // POST: Admin/SubCategory/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var SubCategory = await _context.SubCategory.FindAsync(id);
            _context.SubCategory.Remove(SubCategory);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool SubCategoryExists(int id)
        {
            return _context.SubCategory.Any(e => e.SubCategoryID == id);
        }
    }
}
