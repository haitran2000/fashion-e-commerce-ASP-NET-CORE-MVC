﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Linq.Dynamic.Core;
using Microsoft.EntityFrameworkCore;
using e_Commerce.Data;
using e_Commerce.Models;

namespace e_Commerce.Areas.Admin.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class SubCategoryAPIController : ControllerBase
    {
        private readonly DPContext _context;

        public SubCategoryAPIController(DPContext context)
        {
            _context = context;
        }
        [HttpGet]
        public async Task<ActionResult<IEnumerable<SubCategoryModel>>> GetProducts()
        {
            return await _context.SubCategory.ToListAsync();
        }

        // GET: api/SubCategoryAPI
        [HttpPost]
        public IActionResult GetSubCategories()
        {
            try
            {
                var draw = Request.Form["draw"].FirstOrDefault();
                var start = Request.Form["start"].FirstOrDefault();
                var length = Request.Form["length"].FirstOrDefault();
                var sortColumn = Request.Form["columns[" + Request.Form["order[0][column]"].FirstOrDefault() + "][name]"].FirstOrDefault();
                var sortColumnDirection = Request.Form["order[0][dir]"].FirstOrDefault();
                var searchValue = Request.Form["search[value]"].FirstOrDefault();
                int pageSize = length != null ? Convert.ToInt32(length) : 0;
                int skip = start != null ? Convert.ToInt32(start) : 0;
                int recordsTotal = 0;
                var subcategoriesData = (from p in _context.SubCategory select p);
                if (!(string.IsNullOrEmpty(sortColumn) && string.IsNullOrEmpty(sortColumnDirection)))
                {
                    subcategoriesData = subcategoriesData.OrderBy(sortColumn + " " + sortColumnDirection);
                }
                if (!string.IsNullOrEmpty(searchValue))
                {
                    subcategoriesData = subcategoriesData.Where(m => m.Name.Contains(searchValue));
                }
                recordsTotal = subcategoriesData.Count();
                var data = subcategoriesData.Skip(skip).Take(pageSize).ToList();
                var jsonData = new { draw = draw, recordsFiltered = recordsTotal, recordsTotal = recordsTotal, data = data };
                return Ok(jsonData);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        // PUT: api/SubCategoryAPI/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        public async Task<string> PutSubCategoryModel(int id)
        {
            var subCategoryModel = await _context.SubCategory.FindAsync(id);
            if (id != subCategoryModel.CategoryID)
            {
                return "0";
            }

            _context.Entry(subCategoryModel).State = EntityState.Modified;

            try
            {
                if (subCategoryModel.Status == true)
                {
                    subCategoryModel.Status = false;
                }
                else
                {
                    subCategoryModel.Status = true;
                }
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!SubCategoryModelExists(id))
                {
                    return "0";
                }
                else
                {
                    throw;
                }
            }

            return "{\"id\":" + id + ",\"stt\":\"" + subCategoryModel.Status + "\"}";
        }

        // POST: api/SubCategoryAPI
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        public async Task<ActionResult<SubCategoryModel>> PostSubCategoryModel(SubCategoryModel subCategoryModel)
        {
            _context.SubCategory.Add(subCategoryModel);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetSubCategoryModel", new { id = subCategoryModel.SubCategoryID }, subCategoryModel);
        }

        // DELETE: api/SubCategoryAPI/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<SubCategoryModel>> DeleteSubCategory(int id)
        {
            var subCategory = await _context.SubCategory.FindAsync(id);
            if (subCategory == null)
            {
                return NotFound();
            }

            _context.SubCategory.Remove(subCategory);
            await _context.SaveChangesAsync();

            return subCategory;
        }

        private bool SubCategoryModelExists(int id)
        {
            return _context.SubCategory.Any(e => e.SubCategoryID == id);
        }
    }
}
